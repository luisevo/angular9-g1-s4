import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-private',
  templateUrl: './private.component.html',
  styleUrls: ['./private.component.scss']
})
export class PrivateComponent implements OnInit {
  header = {
    brandUrl: '/spotify',
    brand: 'Spotify App',
    logo: 'assets/logo.png',
    options: [
      { title: 'Buscar', url: '/spotify/search' }
    ]
  }
  constructor() { }

  ngOnInit(): void {
  }

}
