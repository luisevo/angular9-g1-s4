import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';
import { Artist } from '../../interfaces/album-response.interface';
import { ActivatedRoute } from '@angular/router';
import { Track } from '../../interfaces/track.interface';

@Component({
  selector: 'app-artist-detail',
  templateUrl: './artist-detail.component.html',
  styleUrls: ['./artist-detail.component.scss']
})
export class ArtistDetailComponent implements OnInit {
  artist: Artist;
  tracks: Track[] = []

  constructor(
    private spotify: SpotifyService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    this.spotify.getArtist(id)
    .subscribe(
      artist => this.artist = artist,
      err => console.log(err)
    )

    this.spotify.getArtistTracks(id)
    .subscribe(
      tracks => this.tracks = tracks,
      err => console.log(err)
    )
  }

}
