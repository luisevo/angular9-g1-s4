import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';
import { Album } from '../../interfaces/album-response.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-releases',
  templateUrl: './new-releases.component.html',
  styleUrls: ['./new-releases.component.scss']
})
export class NewReleasesComponent implements OnInit {
  albums: Album[] = [];

  constructor(
    private stotify: SpotifyService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.stotify.getNewReleases()
    .subscribe(
      albums => this.albums = albums,
      err => console.log(err),
    )
  }

  goArtist(id: string) {
    this.router.navigate(['/spotify/artist-detail', id]);
    // this.router.navigateByUrl('');
  }

}
