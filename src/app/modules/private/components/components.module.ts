import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicItemComponent } from './music-item/music-item.component';



@NgModule({
  declarations: [MusicItemComponent],
  imports: [
    CommonModule
  ],
  exports: [MusicItemComponent]
})
export class ComponentsModule { }
