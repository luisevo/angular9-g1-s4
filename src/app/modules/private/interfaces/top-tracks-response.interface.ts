import { Track } from './track.interface';

export interface TopTracksReponse {
    tracks: Track[]
}
