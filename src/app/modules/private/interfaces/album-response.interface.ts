export interface Imagen {
    url: string;
}

export interface Artist {
    external_urls: {
        spotify: string;
    },
    id: string;
    name: string;
    images: Imagen[]
}

export interface Album  {
    artists: Artist[];
    images: Imagen[];
    name: string;
};