import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private auth: AuthService,
    private router: Router) { }

  ngOnInit(): void {
  }

  signIn() {
    this.auth.signIn()
    .subscribe(
      token => {
        sessionStorage.setItem('token', token);
        this.router.navigateByUrl('/spotify');
      }
    )
  }

}
