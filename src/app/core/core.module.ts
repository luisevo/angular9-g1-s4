import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicComponent } from './public/public.component';
import { HomeComponent } from './public/home/home.component';
import { PremiumComponent } from './public/premium/premium.component';
import { HelpComponent } from './public/help/help.component';
import { LoginComponent } from './public/login/login.component';
import { RouterModule } from '@angular/router';
import { SharedComponentsModule } from '../shared/components/components.module';
import { ServicesModule } from './services/services.module';



@NgModule({
  declarations: [
    PublicComponent,
    HomeComponent,
    PremiumComponent,
    HelpComponent,
    LoginComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedComponentsModule,
    ServicesModule
  ],
  exports: [
    PublicComponent,
    HomeComponent,
    PremiumComponent,
    HelpComponent,
    LoginComponent
  ]
})
export class CoreModule { }
