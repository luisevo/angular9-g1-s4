import { Injectable } from '@angular/core';
import { ServicesModule } from './services.module';
import { of } from 'rxjs';

@Injectable({
  providedIn: ServicesModule
})
export class AuthService {

  constructor() { }

  signIn() {
    return of('BQA_cLrM39xoMNPdjxdub7OUASoMH0gNFjpXQRhJVeaLyyIueIBk4oqlHvJzYQzINnW3qhY42dHubM9ZHeg');
  }
}
