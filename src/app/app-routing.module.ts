import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicComponent } from './core/public/public.component';
import { HomeComponent } from './core/public/home/home.component';
import { PremiumComponent } from './core/public/premium/premium.component';
import { HelpComponent } from './core/public/help/help.component';
import { LoginComponent } from './core/public/login/login.component';


const routes: Routes = [
  {
    path: '',
    component: PublicComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'premium', component: PremiumComponent },
      { path: 'help', component: HelpComponent },
      { path: 'login', component: LoginComponent },
    ]
  },
  {
    path: 'spotify',
    loadChildren: () => import('./modules/private/private.module').then(m => m.PrivateModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
